console.log("Hello World!");

function trainer(name, pokemon, numOfPokemon) {
        this.name = name;
        this.pokemon = pokemon;
        this.numOfPokemon = numOfPokemon;
        this.description = function () {
                console.log(`${this.name} is a pokemon master.`);
        }
}

let Ash = new trainer("Ash", "Pikachu", 23);
console.log(Ash);
Ash.description();



function Pokemon(name, level, element) {
        this.name = name;
        this.level = level;
        this.element = element;
        this.health = 90;
        this.attack = 80;
        this.ability = function (targetPokemon) {
                console.log(this.name + " " + `tackled ${targetPokemon}`);
                console.log(`${targetPokemon} health is now reduced.`)
        };
        this.status = function () {
                console.log(this.name + " " + "is unable to continue the battle.")
        }
};

let Bulbasaur = new Pokemon("Bulbasaur", 8, "Water");
let Charmander = new Pokemon("Charmander", 7, "Fire");

console.log(Bulbasaur);
console.log(Charmander);
Bulbasaur.ability("Charmander");
Charmander.status();

